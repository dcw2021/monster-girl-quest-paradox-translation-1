
module Graphics
  class << self
    def frame_count=(other)
      @frame_count = other.abs
    end

    alias hima_14_frame_count frame_count
    def frame_count
      hima_14_frame_count + @frame_count
    end
  end
end
